/****
* This is free and unencumbered software released into the public domain.
* 
* Anyone is free to copy, modify, publish, use, compile, sell, or
* distribute this software, either in source code form or as a compiled
* binary, for any purpose, commercial or non-commercial, and by any
* means.
* 
* In jurisdictions that recognize copyright laws, the author or authors
* of this software dedicate any and all copyright interest in the
* software to the public domain. We make this dedication for the benefit
* of the public at large and to the detriment of our heirs and
* successors. We intend this dedication to be an overt act of
* relinquishment in perpetuity of all present and future rights to this
* software under copyright law.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
* IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
* OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
* ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
* OTHER DEALINGS IN THE SOFTWARE.
* 
* For more information, please refer to <http://unlicense.org/>
* 
****/

package xmlpull;

import haxe.io.Input;

/**
 * XML Pull Parser is an interface that defines parsing functionlity provided
 * in <a href="http://www.xmlpull.org/">XMLPULL V1 API</a> (visit this website to
 * learn more about API and its implementations).
 *
 * <p>There are following different
 * kinds of parser depending on which features are set:<ul>
 * <li>behaves like XML 1.0 comliant non-validating parser
 *  <em>if no DOCDECL is present</em> in XML documents when
 *   FEATURE_PROCESS_DOCDECL is false (this is <b>default parser</b>
 *   and internal enetites can still be defiend with defineEntityReplacementText())
 * <li>non-validating parser as defined in XML 1.0 spec when
 *   FEATURE_PROCESS_DOCDECL is true
 * <li>validating parser as defined in XML 1.0 spec when
 *   FEATURE_VALIDATION is true (and that implies that FEATURE_PROCESS_DOCDECL is true)
 * </ul>
 *
 *
 * <p>There are only two key methods: next() and nextToken() that provides
 * access to high level parsing events and to lower level tokens.
 *
 * <p>The parser is always in some event state and type of the current event
 * can be determined by calling
 * <a href="#next()">getEventType()</a> mehod.
 * Initially parser is in <a href="#START_DOCUMENT">START_DOCUMENT</a> state.
 *
 * <p>Method <a href="#next()">next()</a> return int that contains identifier of parsing event.
 * This method can return following events (and will change parser state to the returned event):<dl>
 * <dt><a href="#START_TAG">START_TAG</a><dd> XML start tag was read
 * <dt><a href="#TEXT">TEXT</a><dd> element contents was read and is available via getText()
 * <dt><a href="#END_TAG">END_TAG</a><dd> XML end tag was read
 * <dt><a href="#END_DOCUMENT">END_DOCUMENT</a><dd> no more events is available
 * </dl>
 *
 * <p>For more details on use of API please read
 * Quick Introduction available at <a href="http://www.xmlpull.org">http://www.xmlpull.org</a>
 *
 * @see XmlPullParserFactory
 * @see #defineEntityReplacementText
 * @see #next
 * @see #nextToken
 * @see #FEATURE_PROCESS_DOCDECL
 * @see #FEATURE_VALIDATION
 * @see #START_DOCUMENT
 * @see #START_TAG
 * @see #TEXT
 * @see #END_TAG
 * @see #END_DOCUMENT
 *
 * @author Stefan Haustein
 * @author <a href="http://www.extreme.indiana.edu/~aslom/">Aleksander Slominski</a>
 * @author Jacek Szymanski - adapted to Haxe
 */
interface XmlPullParser {

    /**
     * Use this call to change the general behaviour of the parser,
     * such as namespace processing or doctype declaration handling.
     * This method must be called before the first call to next or
     * nextToken. Otherwise, an exception is trown.
     * <p>Example: call setFeature(FEATURE_PROCESS_NAMESPACES, true) in order
     * to switch on namespace processing. Default settings correspond
     * to properties requested from the XML Pull Parser factory
     * (if none were requested then all feautures are by default false).
     *
     * @exception XmlPullParserException if feature is not supported or can not be set
     * @exception IllegalArgumentException if feature string is null
     */
    public function setFeature(name: String, state: Bool): Void;

    /**
     * Return the current value of the feature with given name.
     * <p><strong>NOTE:</strong> unknown features are <string>always</strong> returned as false
     *
     * @param name The name of feature to be retrieved.
     * @return The value of named feature.
     * @exception IllegalArgumentException if feature string is null
     */

    public function getFeature(name: String): Bool;

    /**
     * Set the value of a property.
     *
     * The property name is any fully-qualified URI.
     */
    public function setProperty(name: String, value: Dynamic): Void;

    /**
     * Look up the value of a property.
     *
     * The property name is any fully-qualified URI. I
     * <p><strong>NOTE:</strong> unknown features are <string>always</strong> returned as null
     *
     * @param name The name of property to be retrieved.
     * @return The value of named property.
     */
    public function getProperty(name: String): Dynamic;


    /**
     * Set the input for parser. Parser event state is set to START_DOCUMENT.
     * Using null parameter will stop parsing and reset parser state
     * allowing parser to free internal resources (such as parsing buffers).
     * 
     * If an input is passed, the parser SHOULD try to determine input encoding.
     */
    public function setInput(input: Input): Void;
    
   /**
     * Sets the input stream the parser is going to process.
     * This call resets the parser state and sets the event type
     * to the initial value START_DOCUMENT.
     *
     * <p><strong>NOTE:</strong> If an input encoding string is passed,
     *  it MUST be used. Otherwise,
     *  if inputEncoding is null, the parser SHOULD try to determine
     *  input encoding following XML 1.0 specification (see below).
     *  If encoding detection is supported then following feature
     *  <a href="http://xmlpull.org/v1/doc/features.html#detect-encoding">http://xmlpull.org/v1/doc/features.html#detect-encoding</a>
     *  MUST be true amd otherwise it must be false
     *
     * @param input contains a raw byte input stream of possibly
     *     unknown encoding (when encoding is null).
     *
     * @param encoding if not null it MUST be used as encoding for input (or error signaled if impossible)
     */
    public function setInputWithEncoding(input: Input, encoding: String): Void;
    
    /**
     * Returns the input encoding if known, null otherwise.
     * If setInput(InputStream, inputEncoding) was called with an inputEncoding
     * value other than null, this value must be returned
     * from this method. Otherwise, if inputEncoding is null and
     * the parser suppports the encoding detection feature
     * (http://xmlpull.org/v1/doc/features.html#detect-encoding),
     * it must return the detected encoding.
     * If setInput(Reader) was called, null is returned.
     * After first call to next if XML declaration was present this method
     * will return encoding declared.
     */
    public function getInputEncoding(): String;

 

    /**
     * Set new value for entity replacement text as defined in
     * <a href="http://www.w3.org/TR/REC-xml#intern-replacement">XML 1.0 Section 4.5
     * Construction of Internal Entity Replacement Text</a>.
     * If FEATURE_PROCESS_DOCDECL or FEATURE_VALIDATION are set then calling this
     * function will reulst in exception because when processing of DOCDECL is enabled
     * there is no need to set manually entity replacement text.
     *
     * <p>The motivation for this function is to allow very small implementations of XMLPULL
     * that will work in J2ME environments and though may not be able to process DOCDECL
     * but still can be made to work with predefined DTDs by using this function to
     * define well known in advance entities.
     * Additionally as XML Schemas are replacing DTDs by allowing parsers not to process DTDs
     * it is possible to create more efficient parser implementations
     * that can be used as underlying layer to do XML schemas validation.
     *
     *
     * <p><b>NOTE:</b> this is replacement text and it is not allowed
     *  to contain any other entity reference
     * <p><b>NOTE:</b> list of pre-defined entites will always contain standard XML
     * entities (such as &amp;amp; &amp;lt; &amp;gt; &amp;quot; &amp;apos;)
     * and they cannot be replaced!
     *
     * @see #setInput
     * @see #FEATURE_PROCESS_DOCDECL
     * @see #FEATURE_VALIDATION
     */
    public function defineEntityReplacementText( entityName: String, replacementText: String): Void;

    /**
     * Return position in stack of first namespace slot for element at passed depth.
     * If namespaces are not enabled it returns always 0.
     * <p><b>NOTE:</b> default namespace is not included in namespace table but
     *  available by getNamespace() and not available from getNamespaceByPrefix(String); default
     *  namespaces are not included in the counts.
     *
     * FIXME this is severely broken. "namespace stack" should be an implementation detail?
     *
     * @see #getNamespacePrefix
     * @see #getNamespaceUri
     * @see #getNamespace()
     * @see #getNamespace(String)
     */
    public function getNamespaceCount(depth: Int): Int;

    /**
     * Return namespace prefixes for position pos in namespace stack
     */
    public function getNamespacePrefix(pos: Int): String;

    /**
     * Return namespace URIs for position pos in namespace stack
     * If pos is out of range it throw exception.
     */
    public function getNamespaceUri(pos: Int): String;

    /**
     * Return uri for the given prefix.
     * It is depending on current state of parser to find
     * what namespace uri is mapped from namespace prefix.
     * For example for 'xsi' if xsi namespace prefix
     * was declared to 'urn:foo' it will return 'urn:foo'.
     *
     * <p>It will return null if namespace could not be found.
     *
     * <p>Convenience method for
     *
     * <pre>
     *  for (int i = getNamespaceCount (getDepth ())-1; i >= 0; i--) {
     *   if (getNamespacePrefix (i).equals (prefix)) {
     *     return getNamespaceUri (i);
     *   }
     *  }
     *  return null;
     * </pre>
     *
     * <p>However parser implementation can be more efficient about.
     *
     * @see #getNamespaceCount
     * @see #getNamespacePrefix
     * @see #getNamespaceUri
     */


    public function getNamespaceByPrefix(prefix: String): String;


    // --------------------------------------------------------------------------
    // miscellaneous reporting methods

    /**
     * Returns the current depth of the element.
     * Outside the root element, the depth is 0. The
     * depth is incremented by 1 when a start tag is reached.
     * The depth is decremented AFTER the end tag
     * event was observed.
     *
     * <pre>
     * &lt;!-- outside --&gt;     0
     * &lt;root>               1
     *   sometext           1
     *     &lt;foobar&gt;         2
     *     &lt;/foobar&gt;        2
     * &lt;/root&gt;              1
     * &lt;!-- outside --&gt;     0
     * &lt;/pre&gt;
     * </pre>
     *
     * All depth-related values (namespace counts, prefixes etc.) are required to be the same for
     * the corresponding start and end tags.
     *
     */
    public function getDepth(): Int;

    /**
     * Short text describing parser position, including a
     * description of the current event and data source if known
     * and if possible what parser was seeing lastly in input.
     * This method is especially useful to give more meaningful error messages.
     */

    public function getPositionDescription(): String;


    /**
     * Current line number: numebering starts from 1.
     */
    public function getLineNumber(): Int;

    /**
     * Current column: numbering starts from 0 (returned when parser is in START_DOCUMENT state!)
     */
    public function getColumnNumber(): Int;


    // --------------------------------------------------------------------------
    // TEXT related methods

    /**
     * Check if current TEXT event contains only whitespace characters.
     * For IGNORABLE_WHITESPACE, this is always true.
     * For TEXT and CDSECT if the current event text contains at lease one non white space
     * character then false is returned. For any other event type exception is thrown.
     *
     * <p><b>NOTE:</b>  non-validating parsers are not
     * able to distinguish whitespace and ignorable whitespace
     * except from whitespace outside the root element. ignorable
     * whitespace is reported as separate event which is exposed
     * via nextToken only.
     *
     * <p><b>NOTE:</b> this function can be only called for element content related events
     * such as TEXT, CDSECT or IGNORABLE_WHITESPACE otherwise
     * exception will be thrown!
     */

    public function isWhitespace(): Bool;

    /**
     * Read text content of the current event as String.
     */

    public function getText(): String;


    /* *
     * Get the buffer that contains text of the current event and
     * start offset of text is passed in first slot of input int array
     * and its length is in second slot.
     *
     * <p><strong>NOTE:</strong> this buffer must not
     * be modified and its content MAY change after call to next() or nextToken().
     *
     * <p><b>NOTE:</b> this methid must return always the same value as getText()
     * and if getText() returns null then this methid returns null as well and
     * values returned in holder MUST be -1 (both start and length).
     *
     * @see #getText
     *
     * @param holderForStartAndLength the 2-element int array into which
     *   values of start offset and length will be written into frist and second slot of array.
     * @return char buffer that contains text of current event
     *  or null if the current event has no text associated.
     */
//    public char[] getTextCharacters(int [] holderForStartAndLength);

    // --------------------------------------------------------------------------
    // START_TAG / END_TAG shared methods

    /**
     * Returns the namespace URI of the current element.
     * If namespaces are NOT enabled, an empty String ("") always is returned.
     * The current event must be START_TAG or END_TAG, otherwise, null is returned.
     */
    public function getNamespace(): String;

    /**
     * Returns the (local) name of the current element
     * when namespaces are enabled
     * or raw name when namespaces are disabled.
     * The current event must be START_TAG or END_TAG, otherwise null is returned.
     * <p><b>NOTE:</b> to reconstruct raw element name
     *  when namespaces are enabled you will need to
     *  add prefix and colon to localName if prefix is not null.
     *
     */
    public function getName(): String;

    /**
     * Returns the prefix of the current element
     * or null if elemet has no prefix (is in defualt namespace).
     *  If namespaces are not enabled it always returns null.
     * If the current event is not  START_TAG or END_TAG the null value is returned.
     */
    public function getPrefix(): String;


    /**
     * Returns true if the current event is START_TAG and the tag is degenerated
     * (e.g. &lt;foobar/&gt;).
     * <p><b>NOTE:</b> if parser is not on START_TAG then the exception will be thrown.
     */
    public function isEmptyElementTag(): Bool;

    // --------------------------------------------------------------------------
    // START_TAG Attributes retrieval methods

    /**
     * Returns the number of attributes on the current element;
     * -1 if the current event is not START_TAG
     *
     * @see #getAttributeNamespace
     * @see #getAttributeName
     * @see #getAttributePrefix
     * @see #getAttributeValue
     */
    public function getAttributeCount(): Int;

    /**
     * Returns the namespace URI of the specified attribute
     *  number index (starts from 0).
     * Returns empty string ("") if namespaces are not enabled or attribute has no namespace.
     * Throws an IndexOutOfBoundsException if the index is out of range
     * or current event type is not START_TAG.
     *
     * <p><strong>NOTE:</p> if FEATURE_REPORT_NAMESPACE_ATTRIBUTES is set
     * then namespace attributes (xmlns:ns='...') amust be reported
     * with namespace
     * <a href="http://www.w3.org/2000/xmlns/">http://www.w3.org/2000/xmlns/</a>
     * (visit this URL for description!).
     * The default namespace attribute (xmlns="...") will be reported with empty namespace.
     * Then xml prefix is bound as defined in
     * <a href="http://www.w3.org/TR/REC-xml-names/#ns-using">Namespaces in XML</a>
     * specification to "http://www.w3.org/XML/1998/namespace".
     *
     * @param zero based index of attribute
     * @return attribute namespace or "" if namesapces processing is not enabled.
     */
    public function getAttributeNamespace(index: Int): String;

    /**
     * Returns the local name of the specified attribute
     * if namespaces are enabled or just attribute name if namespaces are disabled.
     * Throws an IndexOutOfBoundsException if the index is out of range
     * or current event type is not START_TAG.
     *
     * @param zero based index of attribute
     * @return attribute names
     */
    public function getAttributeName(index: Int): String;

    /**
     * Returns the prefix of the specified attribute
     * Returns null if the element has no prefix.
     * If namespaces are disabled it will always return null.
     * Throws an IndexOutOfBoundsException if the index is out of range
     * or current event type is not START_TAG.
     *
     * @param zero based index of attribute
     * @return attribute prefix or null if namesapces processing is not enabled.
     */
    public function getAttributePrefix(index: Int): String;

    /**
     * Returns the type of the specified attribute
     * If parser is non-validating it MUST return CDATA.
     *
     * @param zero based index of attribute
     * @return attribute type (null is never returned)
     */
    public function getAttributeType(index: Int): String;

    /**
     * Returns if the specified attribute was not in input was declared in XML.
     * If parser is non-validating it MUST always return false.
     * This information is part of XML infoset:
     *
     * @param zero based index of attribute
     * @return false if attribute was in input
     */
    public function isAttributeDefault(index: Int): Bool;

    
    /**
     * Returns the given attributes value
     * Throws an IndexOutOfBoundsException if the index is out of range
     * or current event type is not START_TAG.
     *
     * @param zero based index of attribute
     * @return value of attribute
     */
    public function getAttributeValueByIndex(index: Int): String;

    /**
     * Returns the attributes value identified by namespace URI and namespace localName.
     * If namespaces are disbaled namespace must be null.
     * If current event type is not START_TAG then IndexOutOfBoundsException will be thrown.
     *
     * @param namespace Namespace of the attribute if namespaces are enabled otherwise must be null
     * @param name If namespaces enabled local name of attribute otherwise just attribute name
     * @return value of attribute
     */
    public function getAttributeValue(namespace: String, name: String): String;


    // --------------------------------------------------------------------------
    // actual parsing methods

    /**
     * Returns the type of the current event (START_TAG, END_TAG, TEXT, etc.)
     *
     * @see #next()
     * @see #nextToken()
     */
    public function getEventType(): XmlEventType;


    /**
     * Get next parsing event - element content wil be coalesced and only one
     * TEXT event must be returned for whole element content
     * (comments and processing instructions will be ignored and emtity references
     * must be expanded or exception mus be thrown if entity reerence can not be exapnded).
     * If element content is empty (content is "") then no TEXT event will be reported.
     *
     * <p><b>NOTE:</b> empty element (such as &lt;tag/>) will be reported
     *  with  two separate events: START_TAG, END_TAG - it must be so to preserve
     *   parsing equivalency of empty element to &lt;tag>&lt;/tag>.
     *  (see isEmptyElementTag ())
     *
     * @see #isEmptyElementTag
     * @see #START_TAG
     * @see #TEXT
     * @see #END_TAG
     * @see #END_DOCUMENT
     */
    public function next(): XmlEventType;


    /**
     * This method works similarly to next() but will expose
     * additional event types (COMMENT, CDSECT, DOCDECL, ENTITY_REF, PROCESSING_INSTRUCTION, or
     * IGNORABLE_WHITESPACE) if they are available in input.
     *
     * <p>If special feature FEATURE_XML_ROUNDTRIP
     * (identified by URI: http://xmlpull.org/v1/doc/features.html#xml-roundtrip)
     * is true then it is possible to do XML document round trip ie. reproduce
     * exectly on output the XML input using getText().
     *
     * <p>Here is the list of tokens that can be  returned from nextToken()
     * and what getText() and getTextCharacters() returns:<dl>
     * <dt>START_DOCUMENT<dd>null
     * <dt>END_DOCUMENT<dd>null
     * <dt>START_TAG<dd>null
     *   unless FEATURE_XML_ROUNDTRIP enabled and then returns XML tag, ex: &lt;tag attr='val'>
     * <dt>END_TAG<dd>null
     * unless FEATURE_XML_ROUNDTRIP enabled and then returns XML tag, ex: &lt;/tag>
     * <dt>TEXT<dd>return unnormalized element content
     * <dt>IGNORABLE_WHITESPACE<dd>return unnormalized characters
     * <dt>CDSECT<dd>return unnormalized text <em>inside</em> CDATA
     *  ex. 'fo&lt;o' from &lt;!CDATA[fo&lt;o]]>
     * <dt>PROCESSING_INSTRUCTION<dd>return unnormalized PI content ex: 'pi foo' from &lt;?pi foo?>
     * <dt>COMMENT<dd>return comment content ex. 'foo bar' from &lt;!--foo bar-->
     * <dt>ENTITY_REF<dd>return unnormalized text of entity_name (&entity_name;)
     * <br><b>NOTE:</b> it is user responsibility to resolve entity reference
     * <br><b>NOTE:</b> character entities and standard entities such as
     *  &amp;amp; &amp;lt; &amp;gt; &amp;quot; &amp;apos; are reported as well
     * and are not resolved and not reported as TEXT tokens!
     * This requirement is added to allow to do roundtrip of XML documents!
     * <dt>DOCDECL<dd>return inside part of DOCDECL ex. returns:<pre>
     * &quot; titlepage SYSTEM "http://www.foo.bar/dtds/typo.dtd"
     * [&lt;!ENTITY % active.links "INCLUDE">]&quot;</pre>
     * <p>for input document that contained:<pre>
     * &lt;!DOCTYPE titlepage SYSTEM "http://www.foo.bar/dtds/typo.dtd"
     * [&lt;!ENTITY % active.links "INCLUDE">]></pre>
     * </dd>
     * </dl>
     *
     * <p><strong>NOTE:</strong> returned text of token is not end-of-line normalized.
     *
     * @see #next
     * @see #START_TAG
     * @see #TEXT
     * @see #END_TAG
     * @see #END_DOCUMENT
     * @see #COMMENT
     * @see #DOCDECL
     * @see #PROCESSING_INSTRUCTION
     * @see #ENTITY_REF
     * @see #IGNORABLE_WHITESPACE
     */
    public function nextToken(): XmlEventType;

    //-----------------------------------------------------------------------------
    // utility methods to mak XML parsing easier ...

    /**
     * test if the current event is of the given type and if the
     * namespace and name do match. null will match any namespace
     * and any name. If the current event is TEXT with isWhitespace()=
     * true, and the required type is not TEXT, next () is called prior
     * to the test. If the test is not passed, an exception is
     * thrown. The exception text indicates the parser position,
     * the expected event and the current event (not meeting the
     * requirement.
     *
     * <p>essentially it does this
     * <pre>
     *  if (getEventType() == TEXT && type != TEXT && isWhitespace ())
     *    next ();
     *
     *  if (type != getEventType()
     *  || (namespace != null && !namespace.equals (getNamespace ()))
     *  || (name != null && !name.equals (getName ()))
     *     throw new XmlPullParserException ( "expected "+ TYPES[ type ]+getPositionDesctiption());
     * </pre>
     */
    public function require(type: XmlEventType, namespace: String, name: String): Void;


    /**
     * If the current event is text, the value of getText is
     * returned and next() is called. Otherwise, an empty
     * String ("") is returned. Useful for reading element
     * content without needing to performing an additional
     * check if the element is empty.
     *
     * <p>essentially it does this
     * <pre>
     *   if (getEventType != TEXT) return ""
     *   String result = getText ();
     *   next ();
     *   return result;
     * </pre>
     */
    public function readText(): String;

    // FIXME add doc!
    public function nextText(): String;

    /**
     * Call next() and return event if it is START_TAG or END_TAG
     * otherwise throw an exception.
     * It will skip whitespace TEXT before actual tag if any.
     *
     * <p>essentially it does this
     * <pre>
     *   int eventType = next();
     *   if(eventType == TEXT &amp;&amp;  isWhitespace()) {   // skip whitespace
     *      eventType = next();
     *   }
     *   if (eventType != START_TAG &amp;&amp;  eventType != END_TAG) {
     *      throw new XmlPullParserException("expected start or end tag", this, null);
     *   }
     *   return eventType;
     * </pre>
     */
     public function nextTag(): XmlEventType;

 
    
}

