/****
* This is free and unencumbered software released into the public domain.
* 
* Anyone is free to copy, modify, publish, use, compile, sell, or
* distribute this software, either in source code form or as a compiled
* binary, for any purpose, commercial or non-commercial, and by any
* means.
* 
* In jurisdictions that recognize copyright laws, the author or authors
* of this software dedicate any and all copyright interest in the
* software to the public domain. We make this dedication for the benefit
* of the public at large and to the detriment of our heirs and
* successors. We intend this dedication to be an overt act of
* relinquishment in perpetuity of all present and future rights to this
* software under copyright law.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
* IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
* OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
* ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
* OTHER DEALINGS IN THE SOFTWARE.
* 
* For more information, please refer to <http://unlicense.org/>
* 
****/

package xmlpull;

class XmlConstants {

    public static inline var NO_NAMESPACE = "";

    inline public static var PROPERTY_XMLDECL_VERSION = "http://xmlpull.org/v1/doc/properties.html#xmldecl-version";
    inline public static var PROPERTY_XMLDECL_STANDALONE = "http://xmlpull.org/v1/doc/properties.html#xmldecl-standalone";
    inline public static var PROPERTY_XMLDECL_CONTENT = "http://xmlpull.org/v1/doc/properties.html#xmldecl-content";
    
    // ----------------------------------------------------------------------------
    // namespace related features

    /**
     * FEATURE: Processing of namespaces is by default set to false.
     * <p><strong>NOTE:</strong> can not be changed during parsing!
     *
     * @see #getFeature
     * @see #setFeature
     */
    public static inline var FEATURE_PROCESS_NAMESPACES =
        "http://xmlpull.org/v1/doc/features.html#process-namespaces";

    /**
     * FEATURE: Report namespace attributes also - they can be distinguished
     * looking for prefix == "xmlns" or prefix == null and name == "xmlns
     * it is off by default and only meaningful when FEATURE_PROCESS_NAMESPACES feature is on.
     * <p><strong>NOTE:</strong> can not be changed during parsing!
     *
     * @see #getFeature
     * @see #setFeature
     */
    public static inline var FEATURE_REPORT_NAMESPACE_ATTRIBUTES =
        "http://xmlpull.org/v1/doc/features.html#report-namespace-prefixes";

    /**
     * FEATURE: Processing of DOCDECL is by default set to false
     * and if DOCDECL is encountered it is reported by nextToken()
     * and ignored by next().
     *
     * If processing is set to true then DOCDECL must be processed by parser.
     *
     * <p><strong>NOTE:</strong> if the DOCDECL was ignored
     * further in parsing there may be fatal exception when undeclared
     * entity is encountered!
     * <p><strong>NOTE:</strong> can not be changed during parsing!
     *
     * @see #getFeature
     * @see #setFeature
     */
    public static inline var FEATURE_PROCESS_DOCDECL =
        "http://xmlpull.org/v1/doc/features.html#process-docdecl";

    /**
     * FEATURE: Report all validation errors as defined by XML 1.0 sepcification
     * (implies that FEATURE_PROCESS_DOCDECL is true and both internal and external DOCDECL
     * will be processed).
     * <p><strong>NOTE:</strong> can not be changed during parsing!
     *
     * @see #getFeature
     * @see #setFeature
     */
    public static inline var FEATURE_VALIDATION =
        "http://xmlpull.org/v1/doc/features.html#validation";



    /*
     * This feature is identified by http://xmlpull.org/v1/doc/features.html#xml-roundtrip
     * 
     * If set to true then XMLPULL parser MUST report original XML value for START_TAG and END_TAG
     * events with getText() and getTextCharacters() functions.
     * 
     */
    public static inline var FEATURE_XML_ROUNDTRIP = 
      "http://xmlpull.org/v1/doc/features.html#xml-roundtrip";


}
