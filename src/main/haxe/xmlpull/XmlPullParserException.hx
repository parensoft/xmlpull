/****
* This is free and unencumbered software released into the public domain.
* 
* Anyone is free to copy, modify, publish, use, compile, sell, or
* distribute this software, either in source code form or as a compiled
* binary, for any purpose, commercial or non-commercial, and by any
* means.
* 
* In jurisdictions that recognize copyright laws, the author or authors
* of this software dedicate any and all copyright interest in the
* software to the public domain. We make this dedication for the benefit
* of the public at large and to the detriment of our heirs and
* successors. We intend this dedication to be an overt act of
* relinquishment in perpetuity of all present and future rights to this
* software under copyright law.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
* IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
* OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
* ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
* OTHER DEALINGS IN THE SOFTWARE.
* 
* For more information, please refer to <http://unlicense.org/>
* 
****/

package xmlpull;

/**
 * This exception is thrown to signal XML Pull Parser related faults.
 *
 * @author Aleksander Slominski [http://www.extreme.indiana.edu/~aslom/]
 * @author Jacek Szymanski ported to Haxe
 */
class XmlPullParserException {

    public var originalException(default, default): Dynamic;
    public var row(default, null): Int;
    public var column(default, null): Int;
    public var reason(default, null): String;

    public function new(?aReason = "", ?aRow = -1, ?aColumn = -1) {
      reason = aReason;
      row = aRow;
      column = aColumn;
    }
    
    public static function newFromParserState(aMessage: String, aParser: XmlPullParser) {
      var msg = '${aMessage == null ? "" : aMessage}: ${aParser.getPositionDescription()}';

      return new XmlPullParserException(msg, aParser.getLineNumber(), aParser.getColumnNumber());
    }
    
    public function getFullReason(): String {
      if (originalException == null) return reason;
      else return '${reason}\nCause: ${Std.string(originalException)}';
    }

    public function toString() return getFullReason();
    
}

