/****
* This is free and unencumbered software released into the public domain.
* 
* Anyone is free to copy, modify, publish, use, compile, sell, or
* distribute this software, either in source code form or as a compiled
* binary, for any purpose, commercial or non-commercial, and by any
* means.
* 
* In jurisdictions that recognize copyright laws, the author or authors
* of this software dedicate any and all copyright interest in the
* software to the public domain. We make this dedication for the benefit
* of the public at large and to the detriment of our heirs and
* successors. We intend this dedication to be an overt act of
* relinquishment in perpetuity of all present and future rights to this
* software under copyright law.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
* IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
* OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
* ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
* OTHER DEALINGS IN THE SOFTWARE.
* 
* For more information, please refer to <http://unlicense.org/>
* 
****/

package xmlpull;

@:enum abstract XmlEventType(Int) to Int from Int {
    // ----------------------------------------------------------------------------
    // EVENT TYPES as reported by next()
	
	private static var TYPES = [
		"START_DOCUMENT", "END_DOCUMENT", "START_TAG", "END_TAG",
		"TEXT", "CDSECT", "ENTITY_REF", "IGNORABLE_WHITESPACE",
		"PROCESSING_INSTRUCTION", "COMMENT", "DOCDECL"
	];

    /**
     * EVENT TYPE and TOKEN: signalize that parser is at the very beginning of the document
     * and nothing was read yet - the parser is before first call to next() or nextToken()
     * (available from <a href="#next()">next()</a> and <a href="#nextToken()">nextToken()</a>).
     *
     * @see #next
     * @see #nextToken
     */
    public var START_DOCUMENT = 0;

    /**
     * EVENT TYPE and TOKEN: logical end of xml document
     * (available from <a href="#next()">next()</a> and <a href="#nextToken()">nextToken()</a>).
     *
     * <p><strong>NOTE:</strong> calling again
     * <a href="#next()">next()</a> or <a href="#nextToken()">nextToken()</a>
     * will result in exception being thrown.
     *
     * @see #next
     * @see #nextToken
     */
    public var END_DOCUMENT = 1;

    /**
     * EVENT TYPE and TOKEN: start tag was just read
     * (available from <a href="#next()">next()</a> and <a href="#nextToken()">nextToken()</a>).
     * The name of start tag is available from getName(), its namespace and prefix are
     * available from getNamespace() and getPrefix()
     * if <a href='#FEATURE_PROCESS_NAMESPACES'>namespaces are enabled</a>.
     * See getAttribute* methods to retrieve element attributes.
     * See getNamespace* methods to retrieve newly declared namespaces.
     *
     * @see #next
     * @see #nextToken
     * @see #getName
     * @see #getPrefix
     * @see #getNamespace
     * @see #getAttributeCount
     * @see #getDepth
     * @see #getNamespaceCount
     * @see #getNamespace
     * @see #FEATURE_PROCESS_NAMESPACES
     */
    public var START_TAG = 2;

    /**
     * EVENT TYPE and TOKEN: end tag was just read
     * (available from <a href="#next()">next()</a> and <a href="#nextToken()">nextToken()</a>).
     * The name of start tag is available from getName(), its namespace and prefix are
     * available from getNamespace() and getPrefix()
     *
     * @see #next
     * @see #nextToken
     * @see #getName
     * @see #getPrefix
     * @see #getNamespace
     * @see #FEATURE_PROCESS_NAMESPACES
     */
    public var END_TAG = 3;


    /**
     * EVENT TYPE and TOKEN: character data was read and will be available by call to getText()
     * (available from <a href="#next()">next()</a> and <a href="#nextToken()">nextToken()</a>).
     * <p><strong>NOTE:</strong> next() will (in contrast to nextToken ()) accumulate multiple
     * events into one TEXT event, skipping IGNORABLE_WHITESPACE,
     * PROCESSING_INSTRUCTION and COMMENT events.
     * <p><strong>NOTE:</strong> if state was reached by calling next() the text value will
     * be normalized and if the token was returned by nextToken() then getText() will
     * return unnormalized content (no end-of-line normalization - it is content exactly as in
     * input XML)
     *
     * @see #next
     * @see #nextToken
     * @see #getText
     */
    public var TEXT = 4;

    // ----------------------------------------------------------------------------
    // additional events exposed by lower level nextToken()

    /**
     * TOKEN: CDATA sections was just read
     * (this token is available only from <a href="#nextToken()">nextToken()</a>).
     * The value of text inside CDATA section is available  by callling getText().
     *
     * @see #nextToken
     * @see #getText
     */
    public var CDSECT = 5;

    /**
     * TOKEN: Entity reference was just read
     * (this token is available only from <a href="#nextToken()">nextToken()</a>).
     * The entity name is available by calling getText() and it is user responsibility
     * to resolve entity reference.
     *
     * @see #nextToken
     * @see #getText
     */
    public var ENTITY_REF = 6;

    /**
     * TOKEN: Ignorable whitespace was just read
     * (this token is available only from <a href="#nextToken()">nextToken()</a>).
     * For non-validating
     * parsers, this event is only reported by nextToken() when
     * outside the root elment.
     * Validating parsers may be able to detect ignorable whitespace at
     * other locations.
     * The value of ignorable whitespace is available by calling getText()
     *
     * <p><strong>NOTE:</strong> this is different than callinf isWhitespace() method
     *    as element content may be whitespace but may not be ignorable whitespace.
     *
     * @see #nextToken
     * @see #getText
     */
    public var IGNORABLE_WHITESPACE = 7;

    /**
     * TOKEN: XML processing instruction declaration was just read
     * and getText() will return text that is inside processing instruction
     * (this token is available only from <a href="#nextToken()">nextToken()</a>).
     *
     * @see #nextToken
     * @see #getText
     */
    public var PROCESSING_INSTRUCTION = 8;

    /**
     * TOKEN: XML comment was just read and getText() will return value inside comment
     * (this token is available only from <a href="#nextToken()">nextToken()</a>).
     *
     * @see #nextToken
     * @see #getText
     */
    public var COMMENT = 9;

    /**
     * TOKEN: XML DOCTYPE declaration was just read
     * and getText() will return text that is inside DOCDECL
     * (this token is available only from <a href="#nextToken()">nextToken()</a>).
     *
     * @see #nextToken
     * @see #getText
     */
    public var DOCDECL = 10;
	
	public function toString() {
		return TYPES[this];
	}

  public static function getName(ev: XmlEventType) {
    return TYPES[ev];
  }

  public static function forName(aName: String) : XmlEventType {
    for (idx in 0...TYPES.length) {
      if (TYPES[idx] == aName) return (idx: XmlEventType);
    }

    throw new XmlPullParserException('No event named ${aName}');
  }

}
