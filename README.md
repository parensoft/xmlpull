# XML Pull API adapted for the Haxe language.

This is an adaptation of Java's XML Pull API (http://xmlpull.org/) to the Haxe language. This library contains no parser implementation.

There is a parser implementation in the library [girsu](http://lib.haxe.org/p/girsu).

The API itself is in the public domain, both original API at xmlpull.org and this adaptation. The tests have also been ported to Haxe and are available from https://bitbucket.org/parensoft/xmlpull-tests/ (they use utest as its test library). The tests, as required by the original Java tests' license are LGPL-licensed.

### Roadmap:
* Port Serializer API (and an implementation for girsu)
* Add higher-level event-driven API (somewhat inspired by Stax; also implement)

